public class AnimalThread extends Thread {

    AnimalThread(String name, int priority) {
        setName(name);
        setPriority(priority);
    }

    @Override
    public void run() {
        System.out.printf("%s стартовал(а)... Приоритет: %s. \n", getName(), getPriority());

        for (int i = 0; i <= 50000; i++) {
            if (i % 1000 == 0) {
                System.out.printf("%s прошел(а) %d метров. \n", getName(), i);
            }
        }
    }
}
